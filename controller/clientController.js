const Client = require("../model/clientModel");

// Fonction pour créer un nouveau client
const createClient = async (req, res) => {
  try {
    const { nom, prenom, numero, email } = req.body;

    // Vérifier si l'e-mail existe déjà
    const existingEmail = await Client.findOne({ email });

    if (existingEmail) {
      return res.status(404).json({ message: `Cet email existe déjà!` });
    }

    // Vérifier si le numéro de téléphone existe déjà
    const existingNumero = await Client.findOne({ numero });

    if (existingNumero) {
      return res
        .status(404)
        .json({ message: `Ce numéro de téléphone existe déjà!` });
    }

    // Créer un nouveau client
    const client = new Client({ nom, prenom, numero, email });
    await client.save();

    res.status(201).json(client);
  } catch (error) {
    console.error(error);

    res.status(500).json({ message: "Erreur interne du serveur" });
  }
};

// Fonction pour récupérer tous les clients
const getAllClient = async (req, res, next) => {
  try {
    const client = await Client.find();
    res.status(200).json(client);
  } catch (error) {
    res.status(500).json({ message: "Erreur interne du serveur" });
  }
};

// Fonction pour récupérer un client par son ID
// Fonction pour récupérer un client par son ID
const getClientById = async (req, res, next) => {
  try {
    const clientId = req.params.id;

    // Vérifier si l'ID est un nombre entier
    if (!parseInt(clientId)) {
      return res.status(400).json({ message: "Mauvais Paramètre" });
    }

    // Rechercher le client par son ID
    const client = await Client.findById(clientId);

    // Vérifier si le client existe
    if (!client) {
      return res.status(404).json({ message: "Client non trouvé" });
    }

    // Retourner le client trouvé
    res.status(200).json(client);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Erreur interne du serveur" });
  }
};

// Fonction pour mettre à jour un client par son ID
const updateClient = async (req, res, next) => {
  try {
    const clientId = req.params.id;
    const { nom, prenom, numero, email } = req.body;

    // Vérifier si l'ID est un nombre entier
    if (!parseInt(clientId)) {
      return res.status(400).json({ message: "Mauvais Paramètre" });
    }

    // Rechercher le client par son ID
    const client = await Client.findById(clientId);

    // Vérifier si le client existe
    if (!client) {
      return res.status(404).json({ message: "Client non trouvé" });
    }

    // Vérifier si le nouvel email existe déjà pour un autre client
    if (email && email !== client.email) {
      const existingEmail = await Client.findOne({ email });

      if (existingEmail) {
        return res
          .status(400)
          .json({ message: "Cet email existe déjà pour un autre client" });
      }
    }

    // Vérifier si le nouveau numéro de téléphone existe déjà pour un autre client
    if (numero && numero !== client.numero) {
      const existingNumero = await Client.findOne({ numero });

      if (existingNumero) {
        return res
          .status(400)
          .json({
            message: "Ce numéro de téléphone existe déjà pour un autre client",
          });
      }
    }

    // Mettre à jour le client avec les nouvelles informations
    client.nom = nom || client.nom;
    client.prenom = prenom || client.prenom;
    client.numero = numero || client.numero;
    client.email = email || client.email;

    await client.save();

    // Retourner le client mis à jour
    res.status(200).json(client);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Erreur interne du serveur" });
  }
};

// Fonction pour supprimer un client par son ID
// Fonction pour supprimer un client par son ID
const deleteClient = async (req, res) => {
    try {
      const clientId = req.params.id;
  
      // Vérifier si l'ID est un nombre entier
      if (!parseInt(clientId)) {
        return res.status(400).json({ message: "Mauvais Paramètre" });
      }
  
      // Rechercher le client par son ID
      const client = await Client.findById(clientId);
  
      // Vérifier si le client existe
      if (!client) {
        return res.status(404).json({ message: "Client non trouvé" });
      }
  
      // Supprimer le client
      await client.remove();
  
      res.status(204).end(); // 204 No Content (pas de contenu à renvoyer après la suppression)
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Erreur interne du serveur" });
    }
  };
  

module.exports = {
  createClient,
  getAllClient,
  getClientById,
  updateClient,
  deleteClient,
};
