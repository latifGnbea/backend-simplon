const express = require("express");
const { createClient, getAllClient, getClientById, updateClient, deleteClient } = require("../controller/clientController");

const Router = express.Router();

// Routes de chambre en utilisant les fonctions du contrôleur de chambre
Router.post("/create", createClient);
Router.get("/all", getAllClient);
Router.get("/:id", getClientById);
Router.patch("/edit/:id", updateClient);
Router.delete("/delete/:id", deleteClient);

module.exports = Router;
