// import des modules
const express = require("express");
const cors = require("cors");
const port = process.env.SERVER_PORT || 3001;
const clientRouter = require("./router/clientRouter");

// initialisation

const app = express();
app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
    allowedHeaders:
      "Origin, X-Requested-With, x-access-token, role, Content, Accept, Content-Type, Authorization",
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// demarrage de la bd
const dbConnect = require("./config/dbconnect");
dbConnect();

// mise en place du router

app.get("/", (req, res) => res.send("I'm online"));
app.use("/api/client", clientRouter);

app.get("*", (req, res) =>
  res.status(501).send("What the hell are you doing ??/")
);

// demarrage du serveur

app.listen(port, () => {
  console.log(`Demarrage du serveur sur le port ${port}`);
});
